package guru.springframework.ExternalPropertiesDemo;

import exampleBeans.FakeDataSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

@SpringBootApplication
@ComponentScan(basePackages={"config","guru.springframework.ExternalPropertiesDemo"})
public class ExternalPropertiesDemoApplication {

	public static void main(String[] args)
	{
		ApplicationContext ctx=SpringApplication.run(ExternalPropertiesDemoApplication.class, args);
		FakeDataSource fakeDataSource=(FakeDataSource)ctx.getBean(FakeDataSource.class);
	System.out.println(fakeDataSource.getUser());
	System.out.println(fakeDataSource.getPassword());
	System.out.println(fakeDataSource.getUrl());
	}
}
